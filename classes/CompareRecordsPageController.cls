public class CompareRecordsPageController {
    
    public String recordIds {get;set;}
    public String objectApiName {get;set;}
    
    public CompareRecordsPageController(ApexPages.StandardSetController controller) {
        this.objectApiName = String.valueOf(controller.getRecord().getSobjectType());
        this.recordIds = '';
        
        // iterate through selected records and build comma-separated list to pass to Lightning
        for (Sobject obj : controller.getSelected()) {
            this.recordIds += (this.recordIds == '') ? obj.get('Id') : ',' + obj.get('Id');
        }
    }
    
}