public class CompareRecordsController {

    @AuraEnabled
    public static CompareRecordsWrapper getRecordsWrapper(String recordIds,String objectApiName){
        List<Id> recordIdsList = recordIds.split(',');
        return new CompareRecordsWrapper(Database.query(buildQuery(recordIdsList)),objectApiName);
    }
    
    private static String buildQuery(List<Id> recordIds){
        if (recordIds != null && recordIds.size() > 0) {
            String query = 'SELECT ';
            String fieldList = '';
            
            SObjectType objectType = recordIds[0].getSObjectType();
			Map<String,Schema.SObjectField> fields = objectType.getDescribe().fields.getMap();
            for (String field : fields.keySet()) {
                fieldList += (fieldList == '') ? field : ', ' + field;
            }
            
            for (Integer i = 0; i < recordIds.size() ; i++) {
            	recordIds[i] = '\'' + recordIds[i] + '\'';
        	}
            
            query += fieldList;
            query += ' FROM ' + objectType + ' WHERE Id IN ' + recordIds;
            System.debug('query: ' + query);
            return query;
        }
        return null;
    }
}