public class CompareRecordsWrapper {
	@AuraEnabled public List<Map<String,String>> fields {get;set;}
    @AuraEnabled public String recordIds {get;set;}
    @AuraEnabled public List<Sobject> records {get;set;}
    @AuraEnabled public Map<String,Map<Id,String>> fieldToRecordValuesMap {get;set;}
    
    public CompareRecordsWrapper(List<Sobject> recordList,String objectApiName) {
        this.records = recordList;
        this.fields = new List<Map<String,String>>();
        
        SObjectType objectType = Schema.getGlobalDescribe().get(objectApiName);
        Map<String,Schema.SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
        this.fieldToRecordValuesMap = new Map<String,Map<Id,String>>();
        
        for (SobjectField f : fieldMap.values()) {
            Map<String,String> tempMap = new Map<String,String>();
            tempMap.put('name',f.getDescribe().getName());
            tempMap.put('label',f.getDescribe().getLabel());
            fields.add(tempMap);
            
            Map<Id,String> recordIdToFieldValueMap = new Map<Id,String>();
            for (SObject obj : records) {
                recordIdToFieldValueMap.put(Id.valueOf(String.valueOf(obj.get('Id'))),String.valueOf(obj.get(f.getDescribe().getName())));
            }
            this.fieldToRecordValuesMap.put(f.getDescribe().getName(),recordIdToFieldValueMap);
        }
    }
}