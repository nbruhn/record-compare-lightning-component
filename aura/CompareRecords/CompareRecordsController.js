({
	doInit : function(component, event, helper) {
        var recordIds = component.get('v.recordIds');
        
        // get CompareRecordsWrapper object and set as wrapper
        var action = component.get('c.getRecordsWrapper');
        action.setParams({
            'recordIds' : recordIds,
            'objectApiName' : component.get('v.objectApiName')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.wrapper',response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})