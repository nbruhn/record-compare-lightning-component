({
	setRowVisibility : function(component,values) {
        var differentValueFound = false;
        
        if (values.length == 1) {
            // if only one record being displayed, then display row
        	component.set('v.displayRow',true);
        } else {
            var val;
       		for (var i = 0; i < values.length; i++){
            	if (i != values.length - 1){
                    // set differentValueFound to true if current value is different than next value
                	differentValueFound = (values[i] != values[i+1]) ? true : false;
                    if (differentValueFound) {
                        break; 
                    }
            	}
        	}
            component.set('v.displayRow',differentValueFound);
        }
	}
})