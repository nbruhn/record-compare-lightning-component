({
	doInit : function(component, event, helper) {
        var fieldMap = component.get('v.wrapper.fieldToRecordValuesMap');
        var fieldName = component.get('v.field.name');
		var values = [];
        
        var recordList = component.get('v.wrapper.records');

		// iterate through record list to fetch field values for the field being 
		// displayed from all records        
        for (var i = 0; i < recordList.length; i++) {
            values.push(fieldMap[fieldName][recordList[i].Id]);
        }
        component.set('v.values',values);
        
        // determine whether the row should display
        helper.setRowVisibility(component,values);
    }
    
})